<?php

use Drupal\KernelTests\KernelTestBase;
use PHPUnit\Framework\MockObject\MockObject;

/**
 * Test case for the abuseipdb_form_alter function.
 */
class AbuseipdbFormAlterTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['abuseipdb', 'ban'];

  /**
   * Set up the test form array.
   *
   * @param string $form_id
   *   The form ID.
   *
   * @return array
   *   The form array.
   */
  protected function setForm(string $form_id = ''): array {
    return [
      '#form_id' => $form_id ?: 'user_login_form',
      '#validate' => [],
    ];
  }

  /**
   * Set up the test form state.
   *
   * @return \PHPUnit\Framework\MockObject\MockObject
   *   The form state.
   */
  protected function setFormState(): MockObject {
    return $this->createMock('Drupal\Core\Form\FormStateInterface');
  }

  /**
   * Tests the abuseipdb_form_alter function when nothing is set.
   */
  public function testAbuseIpdbFormAlterNoForms() {
    $config = $this->config('abuseipdb.settings');

    // 0 Test empty configuration.
    $form = $this->setForm();
    /** @var \Drupal\Core\Form\FormStateInterface $form_state */
    $form_state = $this->setFormState();
    $config->set('abuseipdb.forms', '');
    $config->save();

    // Call the function to test.
    abuseipdb_form_alter($form, $form_state);

    // Form id.
    $this->assertArrayHasKey('#form_id', $form);
    $this->assertEmpty($form['#validate']);
  }

  /**
   * Tests the abuseipdb_form_alter function.
   */
  public function testAbuseIpdbFormAlterForm() {
    $config = $this->config('abuseipdb.settings');

    // 1 Test user_login_form configuration.
    $form = $this->setForm();
    /** @var \Drupal\Core\Form\FormStateInterface $form_state */
    $form_state = $this->setFormState();
    $config->set('abuseipdb.forms', 'user_login_form');
    $config->save();

    // Call the function to test.
    abuseipdb_form_alter($form, $form_state);

    $this->assertNotEmpty($form['#validate']);
    $this->assertEquals(1, count($form['#validate']));
    $this->assertEquals('abuseipdb_form_validate', $form['#validate'][0]);
  }

  /**
   * Tests the abuseipdb_form_alter function when multiple forms are set.
   */
  public function testAbuseIpdbFormAlterMultipleForms() {
    $config = $this->config('abuseipdb.settings');

    // 2 Test existence of multiple forms.
    $form = $this->setForm();
    /** @var \Drupal\Core\Form\FormStateInterface $form_state */
    $form_state = $this->setFormState();
    $config->set('abuseipdb.forms', 'user_register_form,user_login_form');
    $config->save();

    // Call the function to test.
    abuseipdb_form_alter($form, $form_state);

    $this->assertNotEmpty($form['#validate']);
    $this->assertEquals(1, count($form['#validate']));
    $this->assertEquals('abuseipdb_form_validate', $form['#validate'][0]);
  }

  /**
   * Test a form_id that is not in the configuration.
   */
  public function testAbuseIpdbFormAlterFormIdNotInConfig() {
    $config = $this->config('abuseipdb.settings');

    $form = $this->setForm('user_pass');
    /** @var \Drupal\Core\Form\FormStateInterface $form_state */
    $form_state = $this->setFormState();
    $config->set('abuseipdb.forms', 'user_register_form,user_login_form');
    $config->save();

    // Call the function to test.
    abuseipdb_form_alter($form, $form_state);

    $this->assertEmpty($form['#validate']);
  }

}
