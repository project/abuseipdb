<?php

use Drupal\abuseipdb\ApiClient;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Tests the ApiClient class.
 *
 * @coversDefaultClass \Drupal\abuseipdb\ApiClient
 *
 * @group abuseipdb
 */
class AbuseipdbApiClientTest extends UnitTestCase {

  /**
   * Api client.
   *
   * @var Drupal\abuseipdb\ApiClient
   */
  protected $client;

  /**
   * The http client.
   *
   * @var GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The config factory.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger channel factory.
   *
   * @var Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->httpClient = $this->createMock(Client::class);
    $this->configFactory = $this->createMock(ConfigFactoryInterface::class);
    $this->loggerFactory = $this->createMock(LoggerChannelFactoryInterface::class);
  }

  /**
   * Sets up the API client.
   *
   * @param array $data
   *   The data to return.
   * @param int $response_code
   *   The response code.
   */
  protected function setUpApiClient(array $data, int $response_code = 200): void {
    $response = $this->createMock(ResponseInterface::class);
    $response->method('getStatusCode')
      ->willReturn($response_code);

    $stream = $this->createMock(StreamInterface::class);
    $stream->method('getContents')
      ->willReturn(json_encode($data));

    $response->method('getBody')
      ->willReturn($stream);

    $this->httpClient->expects($this->once())
      ->method('request')
      ->willReturn($response);
  }

  /**
   * Set up the configuration.
   *
   * @param string $api_key
   *   The API key.
   * @param int $shutdown_mode
   *   The shutdown mode.
   * @param string $confidence_score
   *   The confidence score.
   */
  private function setUpConfig(string $api_key = '', int $shutdown_mode = 0, string $confidence_score = '75'): void {
    $this->configFactory = $this->createMock(ConfigFactoryInterface::class);

    $config = $this->getMockBuilder(Config::class)
      ->disableOriginalConstructor()
      ->getMock();

    $config->expects($this->any())
      ->method('get')
      ->willReturnMap([
        ['abuseipdb.shutdown', $shutdown_mode],
        ['abuseipdb.api_key', $api_key],
        ['abuseipdb.abuse_confidence_score', $confidence_score],
      ]);

    $this->configFactory->expects($this->any())
      ->method('get')
      ->with('abuseipdb.settings')
      ->willReturn($config);
  }

  /**
   * Set up logger factory.
   */
  protected function setUpLoggerFactory(): void {
    $this->loggerFactory = $this->createMock(LoggerChannelFactoryInterface::class);

    $loggerChannelInterface = $this->createMock(LoggerChannelInterface::class);

    $loggerChannelInterface->expects($this->any())
      ->method('error')
      ->willReturnCallback(function ($message) {
        return TRUE;
      });

    $this->loggerFactory->expects($this->any())
      ->method('get')
      ->willReturn($loggerChannelInterface);
  }

  /**
   * Tests the constructor.
   *
   * @covers ::__construct
   */
  public function testConstructor(): void {
    $this->setUpConfig('123456');
    $client = new ApiClient($this->httpClient, $this->configFactory, $this->loggerFactory);
    $this->assertInstanceOf(ApiClient::class, $client);
  }

  /**
   * Tests the check method.
   *
   * @covers ::check
   * @covers ::request
   */
  public function testCheck(): void {
    $apiKey = '123456';
    $baseUri = 'https://api.abuseipdb.com/api/v2/';
    $ip = '0.0.0.0';
    $days = 49;
    $data = [
      'data' => [
        'abuseConfidenceScore' => 100,
        'countryCode' => 'US',
        'ipAddress' => $ip,
        'ipVersion' => 4,
        'isWhiteListed' => FALSE,
        'totalReports' => 1001,
      ],
    ];

    $this->setUpConfig($apiKey, 0, '75');
    $this->setUpApiClient($data, 200);

    // $client method 'request' should return $this->httpClient.
    $this->httpClient->expects($this->any())
      ->method('request')
      ->with('GET', 'check', [
        'base_uri' => $baseUri,
        'headers' => [
          'Accept' => 'application/json',
          'Key' => $apiKey,
        ],
        'query' => [
          'days' => $days,
          'ipAddress' => $ip,
        ],
      ]);

    $client = new ApiClient($this->httpClient, $this->configFactory, $this->loggerFactory);
    $response = $client->check($ip, $days);

    $contents = $response->getBody()->getContents();
    $this->assertEquals(200, $response->getStatusCode());
    $this->assertIsString($contents);
    $this->assertEquals($data, json_decode($contents, TRUE));

    $this->tearDown();
  }

  /**
   * Tests the check method with improper API key.
   *
   * @covers ::check
   * @covers ::report
   * @covers ::request
   */
  public function testImproperApiKey(): void {
    $apiKey = '123456';
    $ip = '0.0.0.0';
    $days = 49;

    $this->setUpConfig($apiKey, 0, '75');
    $this->setUpApiClient([
      'errors' => [
        'code' => 'Unauthorized',
        'detail' => 'Invalid API key',
      ],
    ], 401);

    $client = new ApiClient($this->httpClient, $this->configFactory, $this->loggerFactory);
    $response = $client->check($ip, $days);

    $this->assertEquals(401, $response->getStatusCode());
  }

  /**
   * Tests getResponse method.
   *
   * @covers ::check
   * @covers ::request
   * @covers ::getResponse
   */
  public function testGetResponse(): void {
    $apiKey = '123456';
    $ip = '0.0.0.0';
    $days = 49;

    $this->setUpConfig($apiKey);
    $response = $this->createMock(ResponseInterface::class);
    $this->httpClient->expects($this->once())
      ->method('request')
      ->willReturn($response);

    $client = new ApiClient($this->httpClient, $this->configFactory, $this->loggerFactory);

    $client->check($ip, $days);

    $checkResponse = $client->getResponse();

    $this->assertInstanceOf(ResponseInterface::class, $checkResponse);
  }

  /**
   * Tests report method.
   *
   * @covers ::report
   * @covers ::request
   */
  public function testReport(): void {
    $comment = 'Web attack';
    $categories = [15, 21];
    $ip = '0.0.0.0';
    $data = [
      'data' => [
        'ipAddress' => $ip,
        'abuseConfidenceScore' => 0,
      ],
    ];

    $this->setUpConfig('123456');
    $this->setUpApiClient($data, 200);

    $client = new ApiClient($this->httpClient, $this->configFactory, $this->loggerFactory);
    $response = $client->report($ip, $categories, $comment);

    $contents = $response->getBody()->getContents();
    $this->assertEquals(200, $response->getStatusCode());
    $this->assertIsString($contents);
    $this->assertEquals($data, json_decode($contents, TRUE));
  }

  /**
   * Tests the getException method.
   *
   * @covers ::getException
   * @covers ::report
   * @covers ::request
   */
  public function testGetException(): void {
    $comment = 'Web attack';
    $categories = [15, 21];
    $ip = '0.0.0.0';

    $this->setUpConfig('123456');
    $this->setUpApiClient([], 401);
    $this->setUpLoggerFactory();

    // $this->httpClient->request() will throw an exception.
    $this->httpClient->expects($this->once())
      ->method('request')
      ->willThrowException(new RequestException('Unauthorized', new Request('GET', 'check'), new Response(401, [], '{"errors":[{"details":"Unauthorized","detail":"Invalid API key"}]}')));

    $client = new ApiClient($this->httpClient, $this->configFactory, $this->loggerFactory);
    $client->report($ip, $categories, $comment);
    $exception = $client->getException();

    $this->assertInstanceOf(GuzzleException::class, $exception);
  }

  /**
   * Tests the getException method and returns null.
   *
   * @covers ::getException
   */
  public function testGetExceptionNull(): void {
    $comment = 'Web attack';
    $categories = [15, 21];
    $ip = '0.0.0.0';
    $data = [
      'data' => [
        'ipAddress' => $ip,
        'abuseConfidenceScore' => 0,
      ],
    ];

    $this->setUpConfig('123456');
    $this->setUpApiClient($data, 200);

    $client = new ApiClient($this->httpClient, $this->configFactory, $this->loggerFactory);
    $client->report($ip, $categories, $comment);

    $this->assertNull($client->getException());
  }

}
