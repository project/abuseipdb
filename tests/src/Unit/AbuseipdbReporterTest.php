<?php

use Drupal\abuseipdb\ApiClient;
use Drupal\abuseipdb\Reporter;
use Drupal\abuseipdb\ReporterFactory;
use Drupal\ban\BanIpManagerInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Tests\UnitTestCase;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamInterface;

/**
 * Tests the Reporter class.
 *
 * @coversDefaultClass \Drupal\abuseipdb\Reporter
 *
 * @group abuseipdb
 */
class AbuseipdbReporterTest extends UnitTestCase {

  /**
   * Api client.
   *
   * @var Drupal\abuseipdb\ApiClient
   */
  protected $client;

  /**
   * The ban manager.
   *
   * @var Drupal\ban\BanIpManagerInterface
   */
  protected $banManager;

  /**
   * The config factory.
   *
   * @var Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The database connection.
   *
   * @var Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * The current user.
   *
   * @var Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The reporter factory.
   *
   * @var Drupal\abuseipdb\ReporterFactory
   */
  protected $reporterFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->client = $this->createMock(ApiClient::class);
    $this->reporterFactory = $this->createMock(ReporterFactory::class);
    $this->banManager = $this->createMock(BanIpManagerInterface::class);
    $this->configFactory = $this->createMock(ConfigFactoryInterface::class);
    $this->connection = $this->createMock(Connection::class);
    $this->currentUser = $this->createMock(AccountProxyInterface::class);

    $this->setUpConfig();
  }

  /**
   * Set up the configuration.
   *
   * @param string $api_key
   *   The API key.
   * @param int $shutdown_mode
   *   The shutdown mode.
   * @param string $confidence_score
   *   The confidence score.
   */
  private function setUpConfig(string $api_key = '', int $shutdown_mode = 0, string $confidence_score = '75'): void {
    $this->configFactory = $this->createMock(ConfigFactoryInterface::class);

    $config = $this->getMockBuilder(Config::class)
      ->disableOriginalConstructor()
      ->getMock();

    $config->method('get')
      ->willReturnMap([
        ['abuseipdb.shutdown', $shutdown_mode],
        ['abuseipdb.api_key', $api_key],
        ['abuseipdb.abuse_confidence_score', $confidence_score],
      ]);

    $this->configFactory->expects($this->any())
      ->method('get')
      ->with('abuseipdb.settings')
      ->willReturn($config);
  }

  /**
   * Setup the reporter class.
   *
   * @return \Drupal\abuseipdb\Reporter
   *   The reporter class.
   */
  protected function getReporter(): Reporter {
    return new Reporter(
      $this->client,
      $this->reporterFactory,
      $this->configFactory,
      $this->currentUser,
      $this->connection
    );
  }

  /**
   * Set up the API client check response.
   *
   * @param array $data
   *   The data to return.
   * @param int $response_code
   *   The response code.
   */
  private function setUpApiClientCheckResponse(array $data = [], int $response_code = 200): void {
    $stream = $this->createMock(StreamInterface::class);
    $stream->method('getContents')
      ->willReturn(json_encode($data));

    $response = $this->createMock(ResponseInterface::class);
    $response->method('getBody')
      ->willReturn($stream);
    $response->method('getStatusCode')
      ->willReturn($response_code);

    $this->client->method('check')
      ->willReturn($response);
  }

  /**
   * Test the reporter.
   *
   * @covers ::__construct
   */
  public function testConstructor() {
    $this->setUp();

    $reporter = $this->getReporter();

    // Instance of Reporter.
    $this->assertInstanceOf(Reporter::class, $reporter);
  }

  /**
   * Test isBanned.
   *
   * @covers ::isBanned
   */
  public function testResponseBody() {
    $this->setUp();

    $data = ['data' => ['abuseConfidenceScore' => 75]];

    $this->setUpApiClientCheckResponse($data);

    $reporter = $this->getReporter();

    $reporter->check('0.0.0.0', 30);
    $this->assertEquals($reporter->getResponseBody(), json_encode($data));
  }

  /**
   * Test the response is null.
   *
   * @covers ::getResponseBody
   */
  public function testResponseBodyNull() {
    $this->setUp();

    $this->setUpApiClientCheckResponse([]);

    $reporter = $this->getReporter();

    $this->assertEmpty($reporter->getResponseBody());
  }

  /**
   * Test the response code.
   *
   * @covers ::getResponseStatusCode
   */
  public function testResponseStatusCode() {
    $this->setUp();

    $this->setUpApiClientCheckResponse([], 404);

    $reporter = $this->getReporter();

    $reporter->check('0.0.0.0', 30);
    $this->assertEquals($reporter->getResponseStatusCode(), 404);
  }

  /**
   * Test the shutdown mode.
   *
   * @covers ::isShutdownMode
   */
  public function testShutdownMode() {
    $this->setUp();

    $reporter = $this->getReporter();

    $this->assertFalse($reporter->isShutdownMode());

    $this->setUpConfig('key', 1);
    $reporter = $this->getReporter();

    $this->assertTrue($reporter->isShutdownMode());

    $reporter->setShutdownMode(FALSE);
    $this->assertFalse($reporter->isShutdownMode());
  }

  /**
   * Test isAbusive.
   *
   * @covers ::isAbusive
   */
  public function testIsAbusive() {
    $this->setUp();

    $this->setUpApiClientCheckResponse([
      'data' => [
        'abuseConfidenceScore' => 75,
      ],
    ]);

    $reporter = $this->getReporter();

    $this->assertTrue($reporter->isAbusive('0.0.0.0'));
  }

  /**
   * Test isNotAbusive.
   *
   * @covers ::isAbusive
   */
  public function testIsNotAbusive() {
    $this->setUp();

    $this->setUpApiClientCheckResponse([
      'data' => [
        'abuseConfidenceScore' => 74,
      ],
    ]);

    $reporter = $this->getReporter();

    $this->assertFalse($reporter->isAbusive('0.0.0.0'));
  }

}
