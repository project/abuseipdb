<?php

namespace Drupal\abuseipdb_advban;

use Drupal\abuseipdb\AbuseipdbBanManagerInterface;
use Drupal\advban\AdvbanIpManager;

/**
 * Ban Manager for the AdvBan module.
 */
class AdvbanAbuseipdbBanManager implements AbuseipdbBanManagerInterface {

  /**
   * Construct.
   *
   * @param \Drupal\advban\AdvbanIpManagerInterface $banManager
   *   The advanced ban ip manager.
   */
  public function __construct(
    protected AdvbanIpManager $banManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function banIp($ip): void {
    $this->banManager->banIp($ip, '', NULL);
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return 'abuseipdb_advban.ban_manager';
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleName(): string {
    return 'Advanced ban';
  }

  /**
   * {@inheritdoc}
   */
  public function isBanned($ip): bool {
    $advbanResponse = $this->banManager->isBanned($ip, []);

    if (is_bool($advbanResponse)) {
      return $advbanResponse;
    }
    elseif (is_array($advbanResponse)) {
      return count($advbanResponse) > 0;
    }
    else {
      return FALSE;
    }
  }

}
