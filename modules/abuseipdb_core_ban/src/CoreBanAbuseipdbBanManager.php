<?php

namespace Drupal\abuseipdb_core_ban;

use Drupal\abuseipdb\AbuseipdbBanManagerInterface;
use Drupal\ban\BanIpManager;

/**
 * Ban Manager for the Core Ban module.
 */
class CoreBanAbuseipdbBanManager implements AbuseipdbBanManagerInterface {

  /**
   * Construct.
   *
   * @param \Drupal\ban\BanIpManager $banManager
   *   The core ban ip manager.
   */
  public function __construct(
    protected BanIpManager $banManager,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function banIp($ip): void {
    $this->banManager->banIp($ip);
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return 'abuseipdb_core_ban.ban_manager';
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleName(): string {
    return 'Ban (Core)';
  }

  /**
   * {@inheritdoc}
   */
  public function isBanned($ip): bool {
    return $this->banManager->isBanned($ip);
  }

}
