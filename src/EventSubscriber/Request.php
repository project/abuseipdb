<?php

namespace Drupal\abuseipdb\EventSubscriber;

use Drupal\abuseipdb\Reporter;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Checks list of abusive paths and bans the IPs which land there.
 */
class Request implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\Core\Database\Connection $database
   *   Database.
   * @param \Drupal\Core\Path\CurrentPathStack $pathCurrent
   *   Current path.
   * @param \Drupal\Core\Path\PathMatcherInterface $pathMatcher
   *   Path matcher.
   * @param \Drupal\abuseipdb\Reporter $reporter
   *   AbuseIPDB Reporter.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected Connection $database,
    protected CurrentPathStack $pathCurrent,
    protected PathMatcherInterface $pathMatcher,
    protected Reporter $reporter,
    protected RequestStack $requestStack,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::REQUEST][] = ['pathsCheck'];
    return $events;
  }

  /**
   * {@inheritdoc}
   */
  public function pathsCheck(RequestEvent $event) {
    // If user has bypass permissions OR if the shutdown mode is enabled.
    if ($this->reporter->canUserBypass() || $this->reporter->isShutdownMode()) {
      return;
    }

    $abuseIpDbSettings = $this->configFactory->get('abuseipdb.settings');
    $checkList = $abuseIpDbSettings->get('abuseipdb.paths_check');

    // If there are no paths then return.
    if (empty($checkList)) {
      return;
    }

    $currentPath = $this->pathCurrent->getPath();
    $checkListMatch = $this->pathMatcher->matchPath(strtolower($currentPath), strtolower($checkList));

    if ($checkListMatch) {
      $request = $this->requestStack->getCurrentRequest();
      $ip = $request->getClientIp();
      $isAbusive = $this->reporter->isAbusive($ip);

      if ($isAbusive) {
        $banIp = $abuseIpDbSettings->get('abuseipdb.paths_check_ban_ip');
        if ($banIp) {
          $this->reporter->ban($ip);
        }
        else {
          $safePath = $abuseIpDbSettings->get('abuseipdb.paths_check_safe_path') ?? '/';
          $response = new RedirectResponse($safePath);
          $event->setResponse($response);
        }
      }
    }
  }

}
