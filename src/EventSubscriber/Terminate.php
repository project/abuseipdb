<?php

namespace Drupal\abuseipdb\EventSubscriber;

use Drupal\abuseipdb\Reporter;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Path\CurrentPathStack;
use Drupal\Core\Path\PathMatcherInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Checks list of abusive paths and bans the IPs which land there.
 */
class Terminate implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\Core\Database\Connection $database
   *   Database.
   * @param \Drupal\Core\Path\CurrentPathStack $pathCurrent
   *   Current path.
   * @param \Drupal\Core\Path\PathMatcherInterface $pathMatcher
   *   Path matcher.
   * @param \Drupal\abuseipdb\Reporter $reporter
   *   AbuseIPDB Reporter.
   * @param \Symfony\Component\HttpFoundation\RequestStack $requestStack
   *   Request stack.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected Connection $database,
    protected CurrentPathStack $pathCurrent,
    protected PathMatcherInterface $pathMatcher,
    protected Reporter $reporter,
    protected RequestStack $requestStack,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[KernelEvents::TERMINATE][] = ['pathBlacklist'];
    return $events;
  }

  /**
   * {@inheritdoc}
   */
  public function pathBlacklist() {
    // If user has bypass permissions OR if the shutdown mode is enabled.
    if ($this->reporter->canUserBypass() || $this->reporter->isShutdownMode()) {
      return;
    }

    $abuseIpDbSettings = $this->configFactory->get('abuseipdb.settings');
    $blacklist = $abuseIpDbSettings->get('abuseipdb.paths_report');

    // If there are no paths in the blacklist, return.
    if (empty($blacklist)) {
      return;
    }

    $current_path = $this->pathCurrent->getPath();
    $match = $this->pathMatcher->matchPath(strtolower($current_path), strtolower($blacklist));

    if ($match) {
      $request = $this->requestStack->getCurrentRequest();
      $ip = $request->getClientIp();
      $comment = $this->t('[Drupal AbuseIPDB module] Request path is blacklisted. @path', [
        '@path' => $current_path,
      ])->render();

      $this->reporter->report($ip, [21], $comment);

      $ban_ip = $abuseIpDbSettings->get('abuseipdb.paths_report_ban_ip');
      if ($ban_ip) {
        $this->reporter->ban($ip);
      }
    }
  }

}
