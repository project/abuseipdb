<?php

namespace Drupal\abuseipdb;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Factory for creating Reporter instances.
 */
class ReporterFactory {

  /**
   * Create a new Reporter instance.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Drupal\abuseipdb\AbuseipdbBanManagerCollector $banManagerCollector
   *   The ban manager collector.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected AbuseipdbBanManagerCollector $banManagerCollector,
  ) {}

  /**
   * Create a new Reporter instance.
   *
   * @return \Drupal\abuseipdb\AbuseipdbBanManagerInterface
   *   The configured ban manager
   */
  public function doCreate(): AbuseipdbBanManagerInterface {
    $provider = $this->configFactory->get('abuseipdb.settings')->get('abuseipdb.ban_manager') ?? 'abuseipdb.empty_ban_manager';
    return $this->banManagerCollector->getBanManager($provider);
  }

}
