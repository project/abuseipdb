<?php

namespace Drupal\abuseipdb;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Session\AccountProxyInterface;

/**
 * Handles the reporting of IP addresses.
 */
class Reporter {

  /**
   * The Ban Manager for the Ban module.
   *
   * @var \Drupal\ban\BanIpManager
   */
  protected $banManager;

  /**
   * The response from the AbuseIPDB API.
   *
   * @var \GuzzleHttp\Psr7\Response
   */
  protected $response = NULL;

  /**
   * The status of the shutdown setting.
   *
   * @var bool
   */
  protected $shutdown = FALSE;

  /**
   * The status of the exception thrown during the request.
   *
   * @var bool
   */
  protected $exceptionThrown = FALSE;

  /**
   * Constructor.
   *
   * @param \Drupal\abuseipdb\ApiClient $client
   *   The REST API request controller.
   * @param \Drupal\abuseipdb\ReporterFactory $reporterFactory
   *   Reporter factory.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   Current user.
   * @param \Drupal\Core\Database\Connection $database
   *   Database.
   */
  public function __construct(
    protected ApiClient $client,
    protected ReporterFactory $reporterFactory,
    protected ConfigFactoryInterface $configFactory,
    protected AccountProxyInterface $currentUser,
    protected Connection $database,
  ) {
    $this->banManager = $reporterFactory->doCreate();

    // Shutdown mode.
    (bool) $this->shutdown = $this->configFactory->get('abuseipdb.settings')->get('abuseipdb.shutdown');
  }

  /**
   * Ban current IP address.
   *
   * @param string $ip
   *   The IP address to ban.
   */
  public function ban(string $ip): void {
    $this->banManager->banIp($ip);
  }

  /**
   * Check if the current user can bypass checks.
   *
   * @return bool
   *   True if the user can bypass checks and False if the user cannot.
   */
  public function canUserBypass(): bool {
    return $this->currentUser->hasPermission('abuseipdb bypass check');
  }

  /**
   * Check the current IP address.
   *
   * @param string $ip
   *   The IP address to check.
   * @param int $days
   *   The number of days prior to return any reports of abuse.
   */
  public function check(string $ip, int $days): void {
    $this->response = $this->client->check($ip, $days);
  }

  /**
   * Return the response Body from a Report request.
   *
   * @return string|false
   *   The response body from AbuseIPDB API or FALSE if no response.
   */
  public function getResponseBody(): string|false {
    return $this->response ? $this->response->getBody()->getContents() : FALSE;
  }

  /**
   * Return the HTTP Status Code from a Report Request.
   *
   * @return int
   *   The HTTP response code from AbuseIPDB API.
   */
  public function getResponseStatusCode(): int {
    return ($this->response) ? $this->response->getStatusCode() : FALSE;
  }

  /**
   * Check if the current IP is abusive based on AbuseIPDB reports.
   *
   * @param string $ip
   *   The IP address to check.
   *
   * @return bool
   *   True if the IP is abusive and False if the IP is not.
   */
  public function isAbusive(string $ip): bool {
    if (empty($ip)) {
      return FALSE;
    }

    $this->check($ip, 30);

    if ($this->isExceptionThrown() === FALSE && $body = $this->getResponseBody()) {
      $body_array = json_decode($body);
      $abuseIpDbSettings = $this->configFactory->get('abuseipdb.settings');
      $abuseConfidenceScore = $abuseIpDbSettings->get('abuseipdb.abuse_confidence_score');

      if ($body_array->data->abuseConfidenceScore >= (int) $abuseConfidenceScore) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }

    return FALSE;
  }

  /**
   * Check if current IP is banned.
   *
   * @param string $ip
   *   The IP address to check.
   *
   * @return bool
   *   True if the IP is banned and False if the IP is not.
   */
  public function isBanned(string $ip): bool {
    return $this->banManager->isBanned($ip);
  }

  /**
   * Check if an exception was thrown during the request.
   *
   * @return bool
   *   True if an exception was thrown and False if no exception was thrown.
   */
  public function isExceptionThrown(): bool {
    return !is_null($this->client->getException());
  }

  /**
   * Return the status of the shutdown setting.
   *
   * @return bool
   *   The status of the shutdown setting.
   */
  public function isShutdownMode(): bool {
    return $this->shutdown;
  }

  /**
   * Report the current IP address.
   */
  public function report(string $ip, array $categories, string $comment): void {
    $this->response = $this->client->report($ip, $categories, $comment);
  }

  /**
   * Set Shutdown mode.
   *
   * @param bool $shutdown
   *   The shutdown mode.
   */
  public function setShutdownMode(bool $shutdown): void {
    $this->shutdown = $shutdown;
  }

}
