<?php

namespace Drupal\abuseipdb;

/**
 * Empty Ban Manager for the AbuseIPDB module.
 *
 * This class is used when no IP ban module is enabled.
 */
class EmptyAbuseipdbBanManager implements AbuseipdbBanManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function banIp($ip): void {}

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return 'abuseipdb.empty_ban_manager';
  }

  /**
   * {@inheritdoc}
   */
  public function getModuleName(): string {
    return '- None -';
  }

  /**
   * {@inheritdoc}
   */
  public function isBanned($ip): bool {
    return FALSE;
  }

}
