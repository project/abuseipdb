<?php

namespace Drupal\abuseipdb\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Form that tracks a list of pathnames to check.
 */
class PathsCheck extends ConfigFormBase {

  /**
   * Path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   Path matcher.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack.
   */
  public function __construct(PathMatcherInterface $path_matcher, RequestStack $request_stack) {
    $this->pathMatcher = $path_matcher;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('path.matcher'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'abuseipdb_paths_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Form Constructor.
    $form = parent::buildForm($form, $form_state);
    // Default Settings.
    $config = $this->config('abuseipdb.settings');

    $form['instructions'] = [
      '#type' => 'item',
      '#markup' => $this->t('
        <p>This is a list of Drupal paths where traffic should be checked.</p>
        <p>Use carefully as commonly accessed routes will trigger many repeated checks. Consider setting <a href="/admin/people/permissions#module-abuseipdb">Permissions</a> to prevent unnecessary checking of authenticated traffic.</p>
        <p>Module developer <strong>does not</strong> recommend setting check paths on high traffic sites and paths.</p>
      '),
    ];

    $form['paths_check'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Paths'),
      '#default_value' => $config->get('abuseipdb.paths_check'),
      '#description' => $this->t("Specify pages where the visitor IPs should be checked. Enter one path per line. Begin the path with a slash/solidus \"/\". The '*' character is a wildcard."),
      '#required' => FALSE,
    ];

    $form['paths_check_safe_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Safe Path'),
      '#default_value' => $config->get('abuseipdb.paths_check_safe_path'),
      '#description' => $this->t("A Drupal path where it is safe to redirect abuseive IPs."),
      '#required' => FALSE,
    ];

    $form['paths_check_ban_ip'] = [
      '#type' => 'checkbox',
      '#title' => 'Ban IP',
      '#default_value' => $config->get('abuseipdb.paths_check_ban_ip'),
      '#description' => $this->t("Add the IP to Drupal's ban list."),
      '#required' => FALSE,
    ];

    $form['test'] = [
      '#weight' => 1001,
      '#type' => 'textfield',
      '#title' => $this->t('Test a path'),
      '#description' => $this->t('Type a path to test the configuration above. You must Save Configuration to apply changes.'),
      '#required' => FALSE,
      '#field_prefix' => $this->requestStack->getCurrentRequest()->getHost() . '/',
      '#suffix' => '<div id="test-output"></div>',
      '#ajax' => [
        'callback' => '::testPathCallback',
        'disable-refocus' => FALSE,
        'event' => 'keyup',
        'wrapper' => 'test-output',
        'progress' => [
          'type' => 'throbber',
          'checking' => $this->t('looking for match...'),
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('abuseipdb.settings')
      ->set('abuseipdb.paths_check', $form_state->getValue('paths_check'))
      ->set('abuseipdb.paths_check_ban_ip', $form_state->getValue('paths_check_ban_ip'))
      ->set('abuseipdb.paths_check_safe_path', $form_state->getValue('paths_check_safe_path'))
      ->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * Create test field for blacklist.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Render array with markup.
   */
  public function testPathCallback(array &$form, FormStateInterface $form_state) {
    $current_path = '/' . $form_state->getValue('test');
    $blacklist = $form_state->getValue('paths_check');

    $match = $this->pathMatcher->matchPath($current_path, $blacklist);

    return [
      '#markup' => '<div id="test-output">' . ($match !== TRUE ? '&#10007; ' : '&#10003; ') . 'Path is ' . ($match !== TRUE ? '<b>not</b> ' : '') . 'matched</div>',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'abuseipdb.settings',
    ];
  }

}
