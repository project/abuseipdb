<?php

namespace Drupal\abuseipdb\Form;

use Drupal\abuseipdb\AbuseipdbBanManagerCollector;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Module settings.
 */
class Settings extends ConfigFormBase {

  /**
   * The Ban Manager Collector.
   *
   * @var \Drupal\abuseipdb\AbuseipdbBanManagerCollector
   */
  public function __construct(
    protected AbuseipdbBanManagerCollector $banManagerCollector,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('abuseipdb.ban_manager_collector')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'abuseipdb_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form Constructor.
    $form = parent::buildForm($form, $form_state);
    // Default Settings.
    $config = $this->config('abuseipdb.settings');

    $form['bypass'] = [
      '#type' => 'item',
      '#markup' => $this->t('Allow users to bypass checks, reports, and bans. Set in <a href="/admin/people/permissions#module-abuseipdb">Permissions</a>.'),
    ];

    // Api Key field.
    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('AbuseIPDB Api Key:'),
      '#default_value' => $config->get('abuseipdb.api_key'),
      '#description' => $this->t('Your AbuseIPDB account which will be linked to the reports delivered to the database'),
    ];

    $providers = $this->banManagerCollector->getBanManagers();
    foreach ($providers as $provider) {
      $options[$provider->getId()] = $provider->getModuleName();
    }

    if (empty($options)) {
      $this->messenger()->addWarning($this->t('No ban managers found. Please install a module that provides a ban manager.'));
    }

    $form['ban_manager'] = [
      '#type' => 'select',
      '#title' => $this->t('Ban Manager'),
      '#options' => $options,
      '#default_value' => $config->get('abuseipdb.ban_manager'),
      '#description' => $this->t('The module that will be used to ban IPs. If "None" is selected then no IP addresses will be banned.'),
    ];

    $form['abuse_confidence_score'] = [
      '#type' => 'number',
      '#title' => $this->t('Abuse Confidence Score'),
      '#description' => $this->t(
        'The minimum score at which you want to begin banning IPs.
        The Abuse Confidence Score is a 0-100 scale of abusiveness where 0 is
        no abuse and 100 is very abusive.
        <a target="_blank" href="https://docs.abuseipdb.com/?php#blacklist-endpoint">
        AbuseIPDB recommends a value between 75 and 100</a>. <strong>WARNING</strong>
        A value of 0 will ban <u>every</u> IP address.'
      ),
      '#default_value' => $config->get('abuseipdb.abuse_confidence_score'),
      '#min' => 0,
      '#max' => 100,
    ];

    $form['check_timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Check API Timeout'),
      '#description' => $this->t(
        'The maximum time in seconds to wait for a response from the AbuseIPDB API for IP Checks. A timeout value of <strong>0</strong> will wait indefinitely and is the default value.'
      ),
      '#default_value' => $config->get('abuseipdb.check_timeout') ?? 0,
      '#min' => 0,
      '#max' => ini_get('max_execution_time') ? floatval(ini_get('max_execution_time')) : 60,
      '#step' => 0.001,
    ];

    $form['shutdown'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Emergency shutdown'),
      '#default_value' => $config->get('abuseipdb.shutdown'),
      '#description' => $this->t('Disable checks and reports.'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('abuseipdb.settings')
      ->set('abuseipdb.api_key', $form_state->getValue('api_key'))
      ->set('abuseipdb.shutdown', $form_state->getValue('shutdown'))
      ->set('abuseipdb.abuse_confidence_score', $form_state->getValue('abuse_confidence_score'))
      ->set('abuseipdb.ban_manager', $form_state->getValue('ban_manager'))
      ->set('abuseipdb.check_timeout', $form_state->getValue('check_timeout'))
      ->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'abuseipdb.settings',
    ];
  }

}
