<?php

namespace Drupal\abuseipdb\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Form that tracks a list of pathnames to check.
 */
class PathsReport extends ConfigFormBase {

  /**
   * Path matcher.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   Path matcher.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack.
   */
  public function __construct(PathMatcherInterface $path_matcher, RequestStack $request_stack) {
    $this->pathMatcher = $path_matcher;
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('path.matcher'),
      $container->get('request_stack')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'abuseipdb_paths_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Form Constructor.
    $form = parent::buildForm($form, $form_state);
    // Default Settings.
    $config = $this->config('abuseipdb.settings');

    $form['instructions'] = [
      '#type' => 'item',
      '#markup' => $this->t('<p>This is a list of Drupal paths which the site should immediately issue a report to AbuseIPDB for accessing. For example: bot request attempts to Wordpress login/admin routes.</p><p><strong>Do not use on valid Drupal paths lest you report/ban legitimate traffic!</strong></p>'),
    ];

    $form['paths_report'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Paths'),
      '#default_value' => $config->get('abuseipdb.paths_report'),
      '#description' => $this->t("Specify pages where the visitor's ip needs to be reported. Enter one path per line. Begin the path with a slash/solidus \"/\". The '*' character is a wildcard. A good start is /wp-login.php, or /joomla/*. These visitors will be reported to AbuseIPDB in category 'Web App Attack'."),
      '#required' => FALSE,
    ];

    $form['paths_report_ban_ip'] = [
      '#type' => 'checkbox',
      '#title' => 'Ban IP',
      '#default_value' => $config->get('abuseipdb.paths_report_ban_ip'),
      '#description' => $this->t("Add the IP to Drupal's ban list."),
      '#required' => FALSE,
    ];

    $form['test'] = [
      '#weight' => 1001,
      '#type' => 'textfield',
      '#title' => $this->t('Test a path'),
      '#description' => $this->t('Type a path to test the configuration above. You must Save Configuration to apply changes.'),
      '#required' => FALSE,
      '#field_prefix' => $this->requestStack->getCurrentRequest()->getHost() . '/',
      '#suffix' => '<div id="test-output"></div>',
      '#ajax' => [
        'callback' => '::testPathCallback',
        'disable-refocus' => FALSE,
        'event' => 'keyup',
        'wrapper' => 'test-output',
        'progress' => [
          'type' => 'throbber',
          'checking' => $this->t('looking for match...'),
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('abuseipdb.settings')
      ->set('abuseipdb.paths_report', $form_state->getValue('paths_report'))
      ->set('abuseipdb.paths_report_ban_ip', $form_state->getValue('paths_report_ban_ip'))
      ->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * Create test field for blacklist.
   *
   * @param array $form
   *   Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   Render array with markup.
   */
  public function testPathCallback(array &$form, FormStateInterface $form_state) {
    $current_path = '/' . $form_state->getValue('test');
    $blacklist = $form_state->getValue('paths_report');

    $match = $this->pathMatcher->matchPath($current_path, $blacklist);

    return [
      '#markup' => '<div id="test-output">' . ($match !== TRUE ? '&#10007; ' : '&#10003; ') . 'Path is ' . ($match !== TRUE ? '<b>not</b> ' : '') . 'matched</div>',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'abuseipdb.settings',
    ];
  }

}
