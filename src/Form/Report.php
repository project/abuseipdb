<?php

namespace Drupal\abuseipdb\Form;

use Drupal\abuseipdb\Reporter;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * {@inheritdoc}
 */
class Report extends FormBase {

  /**
   * List of abusive categories by which IPs can be recorded.
   *
   * @var array
   */
  protected $abuseIPDBCategories = [];

  /**
   * Drupal\Core\Messenger\MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The reporter service.
   *
   * @var \Drupal\abuseipdb\Reporter
   */
  protected $reporter;

  /**
   * {@inheritdoc}
   */
  public function __construct(MessengerInterface $messenger, Reporter $reporter) {
    foreach (abuseipdb_get_categories_mapping() as $key => $value) {
      $this->abuseIPDBCategories[$key] = $value['title'];
    }

    $this->messenger = $messenger;
    $this->reporter = $reporter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('abuseipdb.reporter'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'abuseipdb_report_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['ip_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('IP address'),
      '#description' => $this->t('The IP address to be reported.'),
      '#required' => TRUE,
    ];

    $form['comment'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Comment'),
      '#description' => $this->t('Why do you want to report?'),
      '#required' => FALSE,
      '#maxlength' => 1499,
    ];

    $form['categories'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Categories'),
      '#description' => $this->t('Select the categories that apply to the report. <a href="https://www.abuseipdb.com/categories" target="_blank">View Categories</a>'),
      '#options' => $this->abuseIPDBCategories,
      '#required' => TRUE,
    ];

    $form['ban_ip'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Ban IP'),
      '#description' => $this->t('Ban the IP address from Drupal.'),
      '#required' => FALSE,
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Report to AbuseIPDB'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validate IP Address.
    $ipAddress = $form_state->getValue('ip_address');
    if (empty($ipAddress)) {
      $form_state->setErrorByName('ip_address', $this->t('Please provide an IP address to report'));
      return;
    }
    elseif (filter_var($ipAddress, FILTER_VALIDATE_IP) === FALSE) {
      $form_state->setErrorByName('ip_address', $this->t('Please provide a valid IP address to report.'));
      return;
    }

    // Validate Categories.
    $categories = $form_state->getValue('categories');
    $categories_keys = array_keys($categories);
    $template_keys = array_keys($this->abuseIPDBCategories);
    if (count(array_diff($categories_keys, $template_keys)) > 0) {
      $form_state->setErrorByName('categories', $this->t('Invalid category found in request.'));
      return;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $ip = trim($form_state->getValue('ip_address'));

    $categories = $this->trimCategoriesArray($form_state->getValue('categories'));

    $this->reporter->report($ip, $categories, trim($form_state->getValue('comment')));

    if ($this->reporter->isExceptionThrown() === TRUE || $this->reporter->getResponseStatusCode() !== 200) {
      $this->messenger()->addMessage($this->t('There was a problem making a Report. Check Logs for more details.', []), 'error', TRUE);
    }
    else {
      $this->messenger()->addMessage($this->t('IP @ip Reported to AbuseIPDB', ['@ip' => $ip]));
    }

    if ($form_state->getValue('ban_ip')) {
      $this->reporter->ban($ip);
      $this->messenger()->addMessage($this->t('IP @ipAddress has been added to Drupal ban list', ['@ipAddress' => $ip]));
    }
  }

  /**
   * Remove empty or 0 values from the categories array.
   *
   * @param array $categories
   *   From the form state.
   *
   * @return array
   *   The cleaned categories array.
   */
  private function trimCategoriesArray(array $categories): array {
    return array_values(array_filter($categories));
  }

}
