<?php

namespace Drupal\abuseipdb;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

/**
 * Handles sending and receiving requests to the AbuseIPDB API.
 */
class ApiClient {

  /**
   * The API key for the AbuseIPDB account.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * AbuseAPIDB base URI.
   *
   * @var string
   */
  protected $baseUri;

  /**
   * The Guzzle client.
   *
   * @var \GuzzleHttp\Client|Null
   */
  protected $exception = NULL;

  /**
   * The response from the API.
   *
   * @var \Psr\Http\Message\ResponseInterface
   */
  protected $response;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    protected Client $client,
    protected ConfigFactoryInterface $configFactory,
    protected LoggerChannelFactoryInterface $loggerFactory,
  ) {
    $currentConfig = $this->configFactory->get('abuseipdb.settings');
    $this->apiKey = $currentConfig->get('abuseipdb.api_key');
    $this->baseUri = 'https://api.abuseipdb.com/api/v2/';
  }

  /**
   * Query the API for IP reports.
   *
   * @param string $ip
   *   The IP to check.
   * @param int $days
   *   The number of days prior to return any reports of abuse.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Guzzle class object.
   */
  public function check(string $ip, int $days = 30): ResponseInterface {
    $query = [
      'days' => $days,
      'ipAddress' => $ip,
    ];

    $this->response = $this->request('GET', 'check', $query);

    return $this->response;
  }

  /**
   * Get the last exception object.
   *
   * @return \GuzzleHttp\Exception\GuzzleException
   *   Guzzle class object.
   */
  public function getException(): ?GuzzleException {
    return $this->exception;
  }

  /**
   * Get the last request object.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Guzzle class object.
   */
  public function getResponse(): ResponseInterface {
    return $this->response;
  }

  /**
   * Handle exceptions thrown by Guzzle.
   *
   * Send Exception message and code to logs.
   *
   * @param \GuzzleHttp\Exception\GuzzleException $e
   *   The exception thrown by Guzzle.
   */
  protected function handleException(GuzzleException $e): void {
    $this->setException($e);

    if ($message = $e->getMessage()) {
      $status = $e->getCode();

      $this->loggerFactory->get('abuseipdb')->error(
        'Status: @status, Error: @errorDetails',
        [
          '@errorDetails' => $message,
          '@status' => $status,
        ],
      );
    }
  }

  /**
   * Check if an error has occurred.
   *
   * @return bool
   *   TRUE if an error has occurred, FALSE otherwise.
   */
  public function hasError(): bool {
    return $this->exception !== NULL;
  }

  /**
   * Delivers a report to AbuseIPDB API.
   *
   * @param string $ip
   *   The IP which is being checked or reported.
   * @param array $categories
   *   Categories for a report. Will be a list of integers.
   * @param string $comment
   *   Optional comment for report.
   *
   * @return \Psr\Http\Message\ResponseInterface
   *   Guzzle class object.
   */
  public function report(string $ip, array $categories, string $comment = ''): ResponseInterface {
    $query = [
      'categories' => implode(',', $categories),
      'comment' => $comment,
      'ip' => $ip,
    ];

    $this->response = $this->request('POST', 'report', $query);

    return $this->response;
  }

  /**
   * Sends a request to the AbuseIPDB API.
   *
   * @param string $method
   *   The HTTP method to use.
   * @param string $action
   *   The API endpoint to send the request to.
   * @param array $query
   *   The query parameters to send with the request.
   *
   * @return Psr\Http\Message\ResponseInterface
   *   The response from the API.
   */
  protected function request(string $method = 'GET', $action = '', array $query = []): ResponseInterface {
    $this->resetException();

    $headers = [
      'Accept' => 'application/json',
      'Key' => $this->apiKey,
    ];

    $options = [
      'base_uri' => $this->baseUri,
      'headers' => $headers,
      'query' => $query,
    ];

    if ($action === 'check') {
      $options['timeout'] = $this->configFactory->get('abuseipdb.settings')->get('abuseipdb.check_timeout') ?? 0;
    }

    try {
      $response = $this->client->request(
        $method,
        $action,
        $options,
      );
    }
    catch (GuzzleException $e) {
      $this->handleException($e);

      // Mock response once Exception is handled.
      $message = $e->getMessage();
      $response = new Response(408, ['Content-Type' => 'plain/text'], $message);
    }

    return $response;
  }

  /**
   * Reset the exception object.
   */
  protected function resetException(): void {
    $this->exception = NULL;
  }

  /**
   * Set the last exception object.
   *
   * @param \GuzzleHttp\Exception\GuzzleException $exception
   *   The exception object.
   */
  public function setException(GuzzleException $exception): void {
    $this->exception = $exception;
  }

}
