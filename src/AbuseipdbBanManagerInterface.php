<?php

namespace Drupal\abuseipdb;

/**
 * An interface to integrate different ban systems with this module.
 */
interface AbuseipdbBanManagerInterface {

  /**
   * Ban an IP address.
   *
   * @param string $ip
   *   The IP address to ban.
   */
  public function banIp($ip): void;

  /**
   * Get the ID of the ban manager.
   *
   * This should match the id in the *.services.yml file.
   *
   * @return string
   *   The ID of the ban manager.
   */
  public function getId(): string;

  /**
   * Get the Module name of the ban manager.
   *
   * @return string
   *   The Module name of the ban manager.
   */
  public function getModuleName(): string;

  /**
   * Check if an IP address is banned.
   *
   * @param string $ip
   *   The IP address to check.
   *
   * @return bool
   *   TRUE if the IP address is banned, FALSE otherwise.
   */
  public function isBanned($ip);

}
