<?php

namespace Drupal\abuseipdb;

/**
 * Find, colllects, and returns a list of AbuseIPDB ban manager services.
 */
class AbuseipdbBanManagerCollector {

  /**
   * Array of ban managers.
   *
   * @var array
   */
  protected $banManagers = [];

  /**
   * Add a service to the collection.
   *
   * @param \Drupal\abuseipdb\AbuseipdbBanManagerInterface $banManager
   *   The ban manager service.
   */
  public function addBanManager(AbuseipdbBanManagerInterface $banManager) {
    $this->banManagers[$banManager->getId()] = $banManager;
  }

  /**
   * Get a ban manager by name.
   *
   * @param string $name
   *   The name of the ban manager.
   *
   * @return \Drupal\abuseipdb\AbuseipdbBanManagerInterface
   *   The ban manager.
   */
  public function getBanManager(string $name): AbuseipdbBanManagerInterface {
    return $this->banManagers[$name];
  }

  /**
   * Get the ban managers.
   *
   * @return array
   *   The collection of ban managers.
   */
  public function getBanManagers(): array {
    uasort($this->banManagers, [$this, 'sortBanManager']);
    return $this->banManagers;
  }

  /**
   * Sort the ban managers.
   *
   * @param \Drupal\abuseipdb\AbuseipdbBanManagerInterface $a
   *   The first ban manager.
   * @param \Drupal\abuseipdb\AbuseipdbBanManagerInterface $b
   *   The second ban manager.
   *
   * @return int
   *   The sort order.
   */
  protected function sortBanManager(AbuseipdbBanManagerInterface $a, AbuseipdbBanManagerInterface $b) {
    if ($a->getId() === '- None -') {
      return 1;
    }

    if ($b->getId() === '- None -') {
      return 1;
    }

    return $a->getModuleName() <=> $b->getModuleName();
  }

}
