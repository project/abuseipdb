# AbuseIPDB module

## What is AbuseIPDB?

AbuseIPDB module is a security project that checks and reports malicious IP addresses across the web.

This module is a connection to the [AbuseIPDB](https://www.abuseipdb.com/) database. It can take a request IP address and check it against the AbuseIPDB database giving the site owner information about "how abusive" the IP address is across the Internet. The module can also send a report to AbuseIPDB to log suspicious and malicious activity to warn others. Optionally the module will log an IP address with Core's Ban module to prevent future traffic from that address.

## Features

This module includes a few features to help secure Drupal from malicious actors:

- Protect Drupal from receiving form submissions.
- Check known request Paths and report crawlers and bot traffic.
- Manually report IP addresses to the AbuseIPDB and ban.

## Installation

1. Enable abuseipdb module.
2. Get an AbuseIPDB API key [here](https://www.abuseipdb.com/register).
3. Go to admin/config/services/abuseipdb to configure the module settings.
4. If you wish to ban malicious IPs from accessing the site when they are detected then enable a submodule like `abuseipdb_core_ban` to utilize Drupal Core Ban module. Alternatively you can use another contributed module if there exists a submodule which implements its service.

## Development

- **3.x** - New features and development.
- **2.x** - Copy of the 8.x-1.x branch. No new development. Will support up to Drupal 11.
